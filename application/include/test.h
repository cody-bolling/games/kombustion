#pragma once

#include <ke/events/mouse-button-event.h>
#include <ke/events/keyboard-event.h>
#include <ke/commands/event-handler-command.h>
#include <ke/events/event-types.h>
#include <ke/base/game-object.h>
#include <ke/graphics/user-interface/imgui/imgui-window.h>
#include <ke/graphics/user-interface/imgui/imgui-log-window.h>
#include <imgui.h>

namespace Main
{
	class MouseEvent : public KE::MouseButtonEvent
	{
		public:
			MouseEvent()
				: KE::MouseButtonEvent((ushort)KE::EventType::MOUSE_BUTTON_PRESSED, (uchar)KE::MouseButton::LEFT_BUTTON) {}

			void function()
			{
				KE::EventHandlerCommand::skipCurrentEventType();
			}
	};

	class KeyEvent : public KE::KeyboardEvent
	{
		private:
			s_ptr<MouseEvent> mouse;
		public:
			KeyEvent(s_ptr<MouseEvent> mouse)
				: KE::KeyboardEvent((ushort)KE::EventType::KEY_RELEASED, (uint)KE::KeyCode::KEY_ENTER),
				  mouse(mouse) {}

			void function()
			{
				KE::EventHandlerCommand::removeEventListener(mouse);
				mouse.reset();
			}
	};

	class TestObject : public KE::GameObject
	{
		public:
			TestObject() {}
			~TestObject() {}
	};
}
