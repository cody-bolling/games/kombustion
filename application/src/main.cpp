#include "test.h"

#include <ke/base/entrypoint.h>
#include <ke/graphics/buffer.h>
#include <ke/graphics/shader.h>

#include <ke/commands/event-handler-command.h>
#include <ke/commands/renderer-command.h>
#include <ke/commands/graphics-api-command.h>

namespace Main
{
	class Application : public KE::Application
	{
		private:
			s_ptr<MouseEvent> mouse1;
			s_ptr<MouseEvent> mouse2;
			s_ptr<KeyEvent> key1;

			u_ptr<KE::VertexArray> vertexArray1;
			u_ptr<KE::VertexArray> vertexArray2;
		public:
			Application()
			{
				/*
				* Setup event listeners
				*/
				mouse1 = std::make_shared<MouseEvent>();
				KE::EventHandlerCommand::addEventListener(mouse1);

				mouse2 = std::make_shared<MouseEvent>();
				KE::EventHandlerCommand::addEventListener(mouse2, 1);

				key1 = std::make_shared<KeyEvent>(mouse2);
				KE::EventHandlerCommand::addEventListener(key1);

				/*
				* Test rendering
				*/
				float vertices1[4 * 7] =
				{
					-0.5f, -0.5f, 0.0f, 0.8f, 0.2f, 0.8f, 1.0f,
					 0.5f, -0.5f, 0.0f, 0.2f, 0.3f, 0.8f, 1.0f,
					 0.5f,  0.5f, 0.0f, 0.8f, 0.8f, 0.2f, 1.0f,
					-0.5f,  0.5f, 0.0f, 0.9f, 0.9f, 0.9f, 1.0f
				};

				uint32_t indices[6] =
				{
					0, 1, 2,
					2, 3, 0
				};

				vertexArray1 = KE::GraphicsAPICommand::createVertexArray();
				vertexArray1->addVertexBuffer(KE::GraphicsAPICommand::createVertexBuffer(vertices1, sizeof(vertices1),
				{
					{ "a_Position", KE::ElementType::FLOAT, 3 },
					{ "a_Color", KE::ElementType::FLOAT, 4 }
				}));
				vertexArray1->setIndexBuffer(KE::GraphicsAPICommand::createIndexBuffer(indices, sizeof(indices) / sizeof(uint32_t)));

				float vertices2[4 * 5] =
				{
					-0.25f, -0.25f, 0.0f, 0.0f, 0.0f,
					 0.25f, -0.25f, 0.0f, 1.0f, 0.0f,
					 0.25f,  0.25f, 0.0f, 1.0f, 1.0f,
					-0.25f,  0.25f, 0.0f, 0.0f, 1.0f,
				};

				vertexArray2 = KE::GraphicsAPICommand::createVertexArray();
				vertexArray2->addVertexBuffer(KE::GraphicsAPICommand::createVertexBuffer(vertices2, sizeof(vertices2),
				{
					{ "a_Position", KE::ElementType::FLOAT, 3 },
					{ "a_TexCoord", KE::ElementType::FLOAT, 2 }
				}));
				vertexArray2->setIndexBuffer(KE::GraphicsAPICommand::createIndexBuffer(indices, sizeof(indices) / sizeof(uint32_t)));

				KE::GraphicsAPICommand::addShaderDirectory("../kombustion/assets/shaders");
			}

			~Application() {}

			virtual inline void update() final override {}

			virtual inline void render() final override
			{
				KE::RendererCommand::tempDrawSquare(*vertexArray2, "texture");
				KE::RendererCommand::tempDrawSquare(*vertexArray1, "test");
			}
	};
}

u_ptr<KE::Application> KE::createApplication()
{
	return std::make_unique<Main::Application>();
}