![Project Avatar](repo-icon.png)

# Kombustion

Kombustion is a game engine written C++ that acts as a collection of packages and APIs to help aid in the development of 2D games.

## Roadmap

This project uses a Trello board called [Kombustion](https://trello.com/b/25oIyoY7/kombustion) to track progress, features, and issues.

## Project Structure

```text
kombustion
├─ README.md
├─ CHANGELOG.md
├─ .gitlab-ci.yml
├─ .gitignore
├─ repo-icon.png
├─ application
│   ├─ include
│   │   └─ test.h
│   └─ src
│       └─ main.cpp
└─ kombustion
    ├─ include
    │   └─ ke
    │       ├─ base
    │       ├─ data-structures
    │       ├─ events
    │       └─ graphics
    ├─ src
    │   ├─ base
    │   ├─ events
    │   └─ graphics
    └─ vendor
        ├─ glad
        ├─ glm
        ├─ imgui
        ├─ sdl
        └─ stb
```
