
engineName = "kombustion"
outputDir = "bin/%{cfg.buildcfg}-%{cfg.architecture}/"

workspace (engineName)
  architecture "x64"
  configurations
  {
    "Debug",
    "Release"
  }

project (engineName)
  location (engineName)
  kind "StaticLib"
  language "C++"
  cppdialect "C++20"
  staticruntime "on"
  targetdir (outputDir .. "%{prj.name}/")
  objdir (outputDir .. "%{prj.name}/intermediate/")

  files
  {
    "%{prj.name}/include/**.h",
    "%{prj.name}/src/**.cpp",
    "%{prj.name}/assets/shaders/**.glsl",
    "%{prj.name}/assets/textures/**.png",

    "%{prj.name}/vendor/glad/**.h",
    "%{prj.name}/vendor/glad/**.c",

    "%{prj.name}/vendor/glm/**.h",
    "%{prj.name}/vendor/glm/**.hpp",
    "%{prj.name}/vendor/glm/**.cpp",
    "%{prj.name}/vendor/glm/**.inl",

    "%{prj.name}/vendor/sdl/**.h",

    "%{prj.name}/vendor/imgui/**.h",
    "%{prj.name}/vendor/imgui/**.cpp",

    "%{prj.name}/vendor/stb/**.h",
    "%{prj.name}/vendor/stb/**.cpp"
  }

  includedirs
  {
    "%{prj.name}/include/",
    "%{prj.name}/vendor/sdl/include/",
    "%{prj.name}/vendor/glad/include/",
    "%{prj.name}/vendor/glm/",
    "%{prj.name}/vendor/imgui/"
  }

  libdirs
  {
    "%{prj.name}/vendor/sdl/lib/"
  }

  links
  {
    "SDL2.lib",
    "SDL2main.lib"
  }

  filter "system:windows"
    systemversion "latest"

  filter "configurations:Debug"
    defines "DEBUG"
    runtime "Debug"
    symbols "on"

  filter "configurations:Release"
    defines "RELEASE"
    runtime "Release"
    optimize "on"

project "application"
  location "application"
  kind "ConsoleApp"
  language "C++"
  cppdialect "C++20"
  staticruntime "on"
  targetdir (outputDir .. "%{prj.name}/")
  objdir (outputDir .. "%{prj.name}/intermediate/")

  files
  {
    "%{prj.name}/include/**.h",
    "%{prj.name}/src/**.cpp",
    "%{prj.name}/assets/shaders/**.glsl",
    "%{prj.name}/assets/textures/**.png"
  }

  includedirs
  {
    "%{prj.name}/include/",
    (engineName .. "/include/"),
    (engineName .. "/vendor/sdl/include/"),
    (engineName .. "/vendor/glm/"),
    (engineName .. "/vendor/imgui/")
  }

  links
  {
    (engineName)
  }

  filter "system:windows"
    systemversion "latest"

    postbuildcommands
    {
      ("{COPYFILE} " .. "../" .. engineName .. "/vendor/sdl/lib/SDL2.dll " .. "../" .. outputDir .. "%{prj.name}/")
    }

  filter "configurations:Debug"
    defines "DEBUG"
    runtime "Debug"
    symbols "on"

  filter "configurations:Release"
    defines "RELEASE"
    runtime "Release"
    optimize "on"
