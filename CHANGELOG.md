# CHANGELOG

## 0.5

- add timer class to accurately measure time steps
  - application is now measuring the delta time of each update cycle takes
- update shader class to read from shader files
- 05302301 check in
  - planning to strip away custom render API after this release and replace with BGFX

## 0.4

- add automatic updates to graphics API viewport when window resizes
- implement ImGui
  - setup to be graphics API agnostic
  - add command wrapper class for the ImGui module so ImGuiWindows can be added from anywhere in the application
- add custom logging window to replace the Windows command prompt
  - set the application project's subsystem to "WINDOWS" instead of "CONSOLE" so the command prompt does not show up
- fix event handler bug where events weren't being added in the right order to their respective list of event listeners

## 0.3

- add Visual Studio project files to the .gitignore since premake can generate them
- update event listener priority system
  - events are now separated from the priority to prevent the application from changing an event's priority while it is in the list of event listeners
- add saftey check in the event handler to prevent adding the same event more than once or removing events not in the handler
- add render pipeline
  - agnostic from the type of graphics API being used
  - from low level to high level: graphics API ->  agnostic graphics API -> renderer -> window -> application
- update console logging to include INFO and WARNING logs
- add proper conversion from Event to SDL_Event
- add command wrapper classes
  - used to allow for static calls of a class's functions
	- makes it easy to limit which functions should be callable by the rest of the application
  - enforces singleton pattern on wrapped classes (but doesn't necessarily have to for future command wrapper classes)
  - used for the following classes
	- Application
	- Event Handler
	- Renderer
	- Graphics API
- add singleton behavior to Window class

## 0.2

- add premake
  - add premake5.lua script and build-project.bat to generate the Visual Studio 2022 project and solution files
- update kombustion project to build as a static library instead of a dynamic library

## 0.1

- add external libraries/packages to vendor directory
  - add glad
  - add glm
  - add imgui
  - add sdl
  - add stb
- add application class
- add game-object class
- add window class
- add event-handler system
  - add keyboard, mouse, window, quit, and custom event types
  - event-handler system is in a stable state marking the first big milestone of the project thus prompting the creation of version 0.1 for this engine
- update README to reflect the above changes

## 0.0

- Initial Commit
