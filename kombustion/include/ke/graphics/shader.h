#pragma once

#include "ke/base/macros.h"

#include <string>
#include <unordered_map>
#include <memory>
#include <filesystem>

namespace KE
{
	enum class UniformType : uchar
	{
		UNDEFINED = 0,
		BOOL,
		INT,
		INT_VECTOR_2,
		INT_VECTOR_3,
		INT_VECTOR_4,
		FLOAT,
		FLOAT_VECTOR_2,
		FLOAT_VECTOR_3,
		FLOAT_VECTOR_4,
		MATRIX_2,
		MATRIX_3,
		MATRIX_4,
		SAMPLER_2D
	};

	struct Uniform
	{
		UniformType type;
		s_ptr<void> data = nullptr;
		int location = 0;
	};

	enum class ShaderType : uchar
	{
		UNDEFINED = 0,
		VERTEX,
		FRAGMENT
	};

	class Shader
	{
		private:
			ShaderType stringToShaderType(const std::string& shaderType);
			void parseUniform(const std::string& line);
		protected:
			u_ptr<std::unordered_map<std::string, Uniform>> uniforms;

			virtual std::unordered_map<ShaderType, std::string> parseShaderSource(const std::string& filePath) final;
			virtual void compile(std::unordered_map<ShaderType, std::string> shaderSources) = 0;
		public:
			inline Shader() {}
			Shader(const std::string& filePath);
			virtual inline ~Shader() {}

			virtual void bind() const = 0;
			virtual void unbind() const = 0;
	};

	class ShaderLibrary
	{
		private:
			u_ptr<std::unordered_map<std::string, s_ptr<Shader>>> shaders;
		public:
			ShaderLibrary();
			virtual inline ~ShaderLibrary() {}

			virtual s_ptr<Shader> createShader(std::string filePath) = 0;
			virtual void addShader(std::string filePath) final;
			virtual void addShaderDirectory(std::string directoryPath) final;
			virtual s_ptr<Shader> getShader(std::string shaderName) final;
	};
}
