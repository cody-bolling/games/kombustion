#pragma once

#include "ke/base/macros.h"

#include <string>

namespace KE
{
	class Texture2D
	{
		protected:
			uint width;
			uint height;

		public:
			Texture2D(const std::string& filePath);
			virtual ~Texture2D();
	};
}