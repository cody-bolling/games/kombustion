#pragma once

namespace KE
{
	class ImGuiWindow
	{
		public:
			virtual void render() = 0;
	};
}