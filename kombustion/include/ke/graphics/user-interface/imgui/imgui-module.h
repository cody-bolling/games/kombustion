#pragma once

#include "ke/graphics/user-interface/imgui/imgui-window.h"
#include "ke/base/macros.h"

#include <imgui.h>
#include <SDL_events.h>
#include <vector>
#include <queue>
#include <memory>

namespace KE
{
	struct ImGuiWindowOperation
	{
		bool add;
		s_ptr<ImGuiWindow> window;
	};

	class ImGuiModule
	{
		private:
			u_ptr<std::queue<ImGuiWindowOperation>> pendingOperations;
			u_ptr<std::vector<s_ptr<ImGuiWindow>>> windows;

			virtual ImGuiIO& init() final;
		protected:
			ImGuiIO& io;

			virtual void preRender() = 0;
			virtual void postRender() = 0;
			virtual void runPendingOperations() final;
		public:
			ImGuiModule();
			virtual ~ImGuiModule();

			virtual void render() final;
			virtual bool processEvent(SDL_Event* sdlEvent) final;
			virtual void addImGuiWindow(s_ptr<ImGuiWindow> window) final;
			virtual void removeImGuiWindow(s_ptr<ImGuiWindow> window) final;
	};
}
