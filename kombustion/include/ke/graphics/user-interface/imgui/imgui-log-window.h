#pragma once

#include "ke/graphics/user-interface/imgui/imgui-window.h"

#include <imgui.h>
#include <string>

namespace KE
{
    class LogWindow : public ImGuiWindow
    {
        private:
            ImGuiWindowFlags windowFlags;
            ImGuiTextBuffer Buf;
            ImGuiTextFilter Filter;
            ImVector<int> LineOffsets;
        public:
            LogWindow();
            ~LogWindow();

            void clear();
            void log(std::string log);
            virtual void render() final override;
    };
}