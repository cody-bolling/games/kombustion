#pragma once

#include "ke/graphics/user-interface/imgui/imgui-module.h"
#include "ke/graphics/opengl/opengl-window.h"

#include <imgui_impl_sdl.h>
#include <imgui_impl_opengl3.h>

namespace KE
{
	class OpenGLImGuiModule : public ImGuiModule
	{
		private:
			OpenGLWindow* openglWindow;
		protected:
			virtual void preRender() final override;
			virtual void postRender() final override;
		public:
			OpenGLImGuiModule(OpenGLWindow* openglWindow);
			~OpenGLImGuiModule();
	};
}