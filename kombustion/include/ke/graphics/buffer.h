#pragma once

#include "ke/base/macros.h"

#include <memory>
#include <string>
#include <vector>

namespace KE
{
	enum class ElementType : uchar
	{
		UNDEFINED = 0,
		BOOL,
		INT,
		FLOAT
	};

	struct BufferElement
	{
		std::string name;
		ElementType type;
		uint32_t datumSize;
		uint32_t datumCount;
		bool normalized;
		uint32_t offset;

		BufferElement();
		BufferElement(const std::string& name, ElementType type, uint32_t datumCount, bool normalized = false);
	};

	class VertexBuffer
	{
		protected:
			std::vector<BufferElement> bufferLayout;
			uint32_t stride;

			virtual void setBufferLayoutOffsets() final;
		public:
			VertexBuffer(float* vertices, uint32_t size, std::initializer_list<BufferElement> elements);
			virtual ~VertexBuffer();

			virtual void bind() const = 0;
			virtual void unbind() const = 0;
			virtual inline uint32_t getBufferLayoutStride() final { return stride; }
			virtual inline std::vector<BufferElement>::iterator getBufferLayoutIteratorBegin() final { return bufferLayout.begin();}
			virtual inline std::vector<BufferElement>::iterator getBufferLayoutIteratorEnd() final { return bufferLayout.end(); }
	};

	class IndexBuffer
	{
		private:
			uint32_t count;
		public:
			IndexBuffer(uint32_t* indices, uint32_t count);
			virtual ~IndexBuffer();

			virtual void bind() const = 0;
			virtual void unbind() const = 0;
	};

	class VertexArray
	{
		protected:
			std::vector<s_ptr<VertexBuffer>> vertexBuffers;
			s_ptr<IndexBuffer> indexBuffer;
			uint vertexBufferElementCount;
		public:
			VertexArray();
			virtual ~VertexArray();

			virtual void bind() const = 0;
			virtual void unbind() const = 0;
			virtual void addVertexBuffer(const s_ptr<VertexBuffer>& vertexBuffer) = 0;
			virtual void setIndexBuffer(const s_ptr<IndexBuffer>& indexBuffer) = 0;
	};
}
