#pragma once

#include "ke/base/macros.h"
#include "ke/graphics/renderer.h"
#include "ke/events/window-event.h"
#include "ke/events/event-types.h"
#include "ke/commands/graphics-api-command.h"
#include "ke/graphics/user-interface/imgui/imgui-module.h"

#include <memory>
#include <SDL.h>
#include <SDL_main.h>

namespace KE
{
	class Window
	{
		private:
			static bool initialized;
		protected:
			SDL_Window* window;
			u_ptr<Renderer> renderer;
			u_ptr<ImGuiModule> imguiModule;
			s_ptr<WindowEvent> windowResizeEvent;
		public:
			Window();
			virtual ~Window();

			static u_ptr<Window> create(GraphicsPlatform graphicsPlatform);

			virtual void preRender();
			virtual void postRender();
			virtual inline SDL_Window* getSDLWindow() final { return window; };
	};
}