#pragma once

#include "ke/base/macros.h"
#include "ke/graphics/graphics-api.h"
#include "ke/graphics/opengl/opengl-shader.h"

#include <memory>

namespace KE
{
	class OpenGL : public GraphicsAPI
	{
		public:
			OpenGL();
			virtual inline ~OpenGL() {}

			virtual void setClearColor(const glm::vec4& color) final override;
			virtual void clear() final override;
			virtual void setViewport(uint32_t x, uint32_t y, uint32_t width, uint32_t height);
			virtual u_ptr<VertexBuffer> createVertexBuffer(float* vertices, uint32_t size, std::initializer_list<BufferElement> elements) final override;
			virtual u_ptr<IndexBuffer> createIndexBuffer(uint32_t* indices, uint32_t count) final override;
			virtual u_ptr<VertexArray> createVertexArray() final override;
			virtual void addShader(const std::string& filePath) final override;
			virtual void addShaderDirectory(const std::string& directoryPath) final override;
			virtual s_ptr<Shader> getShader(const std::string& name) final override;
	};
}
