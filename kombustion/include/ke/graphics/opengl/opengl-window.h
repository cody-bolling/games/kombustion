#pragma once

#include "ke/graphics/window.h"

#include <glad/glad.h>

namespace KE
{
	class OpenGLWindow : public Window
	{
		private:
			SDL_GLContext context;

			void initSDL();
			void initGlad();
		public:
			OpenGLWindow();
			~OpenGLWindow();

			virtual void postRender() final override;
			virtual inline SDL_GLContext getSDLOpenGLContext() final { return context; };
	};
}