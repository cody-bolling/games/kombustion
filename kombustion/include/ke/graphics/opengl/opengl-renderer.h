#pragma once

#include "ke/graphics/renderer.h"

namespace KE
{
	class OpenGLRenderer : public Renderer
	{
		public:
			OpenGLRenderer();
			~OpenGLRenderer();

			virtual void tempDrawSquare(const VertexArray& vertexArray, const std::string& shaderName) final override;
	};
}