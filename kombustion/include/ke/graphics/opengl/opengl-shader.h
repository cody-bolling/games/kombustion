#pragma once

#include "ke/graphics/shader.h"

#include <glad/glad.h>
#include <vector>
#include <string>

namespace KE
{
	class OpenGLShader : public Shader
	{
		private:
			uint32_t rendererID;

			void checkShaderCompileStatus(std::vector<GLuint> shaders);
			void checkShaderLinkStatus(GLuint program, std::vector<GLuint> shaders);
		protected:
			virtual void compile(std::unordered_map<ShaderType, std::string> shaderSources) final override;
		public:
			OpenGLShader(const std::string& filePath);
			~OpenGLShader();

			virtual void bind() const final override;
			virtual void unbind() const final override;
	};

	class OpenGLShaderLibrary : public ShaderLibrary
	{
		public:
			inline OpenGLShaderLibrary() {};
			inline ~OpenGLShaderLibrary() {};

			virtual s_ptr<Shader> createShader(std::string filePath) final;
	};
}