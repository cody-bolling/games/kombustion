#pragma once

#include "ke/graphics/buffer.h"

#include <glad/glad.h>

namespace KE
{
	class OpenGLVertexBuffer : public VertexBuffer
	{
		private:
			uint32_t rendererID;
		public:
			OpenGLVertexBuffer(float* vertices, uint32_t size, std::initializer_list<BufferElement> elements);
			virtual ~OpenGLVertexBuffer();

			virtual void bind() const final override;
			virtual void unbind() const final override;
	};

	class OpenGLIndexBuffer : public IndexBuffer
	{
		private:
			uint32_t rendererID;
		public:
			OpenGLIndexBuffer(uint32_t* indices, uint32_t count);
			~OpenGLIndexBuffer();

			virtual void bind() const final override;
			virtual void unbind() const final override;
	};

	class OpenGLVertexArray : public VertexArray
	{
		private:
			uint32_t rendererID;

			virtual GLenum getGLenumType(ElementType type) final;
		public:
			OpenGLVertexArray();
			virtual ~OpenGLVertexArray();

			virtual void bind() const final override;
			virtual void unbind() const final override;
			virtual void addVertexBuffer(const s_ptr<VertexBuffer>& vertexBuffer) final override;
			virtual void setIndexBuffer(const s_ptr<IndexBuffer>& indexBuffer) final override;
	};
}
