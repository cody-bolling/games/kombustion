#pragma once

#include "ke/graphics/buffer.h"
#include "ke/graphics/shader.h"
#include "ke/graphics/graphics-api.h"

#include <string>

namespace KE
{
	class Renderer
	{
		protected:
			u_ptr<GraphicsAPI> graphicsAPI;
		public:
			Renderer();
			~Renderer();

			virtual void clear() final;
			virtual void tempDrawSquare(const VertexArray& vertexArray, const std::string& shaderName) = 0;
	};
}