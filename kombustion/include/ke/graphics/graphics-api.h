#pragma once

#include "ke/base/macros.h"
#include "ke/graphics/buffer.h"
#include "ke/graphics/shader.h"

#include <memory>
#include <glm.hpp>

namespace KE
{
	enum class GraphicsPlatform : uchar
	{
		UNDEFINED = 0,
		OPENGL
	};

	class GraphicsAPI
	{
		protected:
			u_ptr<ShaderLibrary> shaderLibrary;
		public:
			GraphicsAPI();
			virtual ~GraphicsAPI();

			virtual void setClearColor(const glm::vec4& color) = 0;
			virtual void clear() = 0;
			virtual void setViewport(uint32_t x, uint32_t y, uint32_t width, uint32_t height) = 0;
			virtual u_ptr<VertexBuffer> createVertexBuffer(float* vertices, uint32_t size, std::initializer_list<BufferElement> elements) = 0;
			virtual u_ptr<IndexBuffer> createIndexBuffer(uint32_t* indices, uint32_t count) = 0;
			virtual u_ptr<VertexArray> createVertexArray() = 0;
			virtual void addShader(const std::string& filePath) = 0;
			virtual void addShaderDirectory(const std::string& directoryPath) = 0;
			virtual s_ptr<Shader> getShader(const std::string& name) = 0;
	};
}
