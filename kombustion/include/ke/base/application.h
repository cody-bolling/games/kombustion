#pragma once

#include "ke/base/macros.h"
#include "ke/events/event-handler.h"
#include "ke/events/quit-event.h"
#include "ke/events/window-event.h"
#include "ke/graphics/window.h"
#include "ke/graphics/user-interface/imgui/imgui-log-window.h"
#include "ke/base/timer.h"

#include <memory>
#include <string>

namespace KE
{
	class Application
	{
		private:
			bool running;
			s_ptr<LogWindow> logWindow;
			u_ptr<EventHandler> eventHandler;
			s_ptr<QuitEvent> quitEvent;
			s_ptr<WindowEvent> windowCloseEvent;
			u_ptr<Window> window;
			u_ptr<Timer> updateTimer;
			float maxUpdateDeltaTime;
		protected:
			float updateDeltaTime;

			virtual void update() = 0;
			virtual void render() = 0;
		public:
			Application();
			virtual ~Application();

			virtual void run() final;
			virtual void stop() final;
			virtual void log(std::string log) final;
	};

	u_ptr<Application> createApplication();
}
