#pragma once

#include <chrono>

namespace KE
{
	class Timer
	{
		private:
			std::chrono::time_point<std::chrono::high_resolution_clock> start;
		public:
			inline Timer() { reset(); }

			inline void reset() { start = std::chrono::high_resolution_clock::now(); }
			inline float elapsed() const { return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - start).count() * 0.0000001f * 0.01f; }
	};
}
