#pragma once

#include "ke/base/macros.h"
#include "ke/base/application.h"

int WinMain(int argc, char* argv[])
{
	u_ptr<KE::Application> application = KE::createApplication();
	application->run();
}
