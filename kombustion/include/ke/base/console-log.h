#pragma once

#include "ke/commands/application-command.h"

#include <sstream>
#include <cassert>

namespace KE
{
	template<typename Arg, typename... Args>
	static inline void infoLog(Arg&& arg, Args&&... args)
	{
		std::stringstream stream;

		stream << "[INFO] ";
		stream << std::forward<Arg>(arg);
		((stream << " " << std::forward<Args>(args)), ...);
		stream << "\n";

		ApplicationCommand::log(stream.str());
	}

	template<typename Arg, typename... Args>
	static inline void warningLog(Arg&& arg, Args&&... args)
	{
		std::stringstream stream;

		stream << "[WARNING] ";
		stream << std::forward<Arg>(arg);
		((stream << " " << std::forward<Args>(args)), ...);
		stream << "\n";

		ApplicationCommand::log(stream.str());
	}

	template<typename Arg, typename... Args>
	static inline void errorLog(Arg&& arg, Args&&... args)
	{
		std::stringstream stream;

		stream << "[ERROR] ";
		stream << std::forward<Arg>(arg);
		((stream << " " << std::forward<Args>(args)), ...);
		stream << "\n";

		ApplicationCommand::log(stream.str());
		assert(false);
	}
}
