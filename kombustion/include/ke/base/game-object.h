#pragma once

#include "ke/base/macros.h"

namespace KE
{
	class GameObject
	{
		private:
			static uint nextID;
			static inline uint generateID() { return nextID++; }

			const uint id;
		public:
			GameObject();
			virtual ~GameObject();

			virtual uint getID() final;
	};
}
