#pragma once

#ifndef uchar
	#define uchar unsigned char
#endif

#ifndef ushort
	#define ushort unsigned short
#endif

#ifndef uint
	#define uint unsigned int
#endif

#ifndef u_ptr
	#define u_ptr std::unique_ptr
#endif

#ifndef s_ptr
	#define s_ptr std::shared_ptr
#endif

#ifndef SDL_main_h_
	#define SDL_main_h_
#endif

#ifndef IMGUI_IMPL_OPENGL_LOADER_CUSTOM
	#define IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif
