#pragma once

#include "ke/events/event.h"
#include "SDL_events.h"

namespace KE
{
	class QuitEvent : public Event
	{
		public:
			QuitEvent();
			~QuitEvent();

			virtual void function() final override;
	};
}