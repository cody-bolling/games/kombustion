#pragma once

#include "ke/events/event.h"
#include "SDL_events.h"

namespace KE
{
	class MouseMotionEvent : public Event
	{
		protected:
			int x;
			int y;
		public:
			MouseMotionEvent();
			~MouseMotionEvent();

			virtual bool isTriggered(const SDL_Event& sdlEvent) final override;
	};
}