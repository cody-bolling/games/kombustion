#pragma once

#include "ke/base/macros.h"
#include "ke/events/event.h"
#include "SDL_events.h"

namespace KE
{
	class KeyboardEvent : public KE::Event
	{
		private:
			static Uint16 modsToIgnore[6];

			virtual void validateType() final;
			virtual ushort ignoreModKeys(Uint16 mod) final;
		protected:
			uint keycode;
			ushort mod;
		public:
			KeyboardEvent();
			KeyboardEvent(ushort type, uint keycode);
			KeyboardEvent(ushort type, uint keycode, ushort mod);
			~KeyboardEvent();

			virtual operator SDL_Event() final override;
			virtual bool isTriggered(const SDL_Event& sdlEvent) final override;
	};
}