#pragma once

#include "ke/base/macros.h"
#include "SDL_events.h"

namespace KE
{
	class Event
	{
		protected:
			ushort type;
			bool disabled;
		public:
			Event();
			Event(ushort type);
			~Event();

			virtual void enable() final;
			virtual void disable() final;
			virtual bool isEnabled() final;
			virtual ushort getType() final;
			virtual operator SDL_Event();
			virtual bool isTriggered(const SDL_Event& sdlEvent);
			virtual void function() = 0;
	};
}
