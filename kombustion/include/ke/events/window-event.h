#pragma once

#include "ke/base/macros.h"
#include "ke/events/event.h"
#include "SDL_events.h"

namespace KE
{
	class WindowEvent : public Event
	{
		protected:
			uchar windowEventType;
			int data1;
			int data2;
		public:
			WindowEvent();
			WindowEvent(uchar windowEventType);
			~WindowEvent();

			virtual bool isTriggered(const SDL_Event& sdlEvent) final override;
	};
}