#pragma once

#include "ke/base/macros.h"
#include "ke/events/event.h"
#include "ke/events/event-types.h"

#include <unordered_map>
#include <vector>
#include <queue>
#include <memory>

/*
* NOTE: The event handler does not handle the changing of an event's priorty while it is in the list of event listeners.
*		The current work-around is to remove the event, change it's priority, and add the event back.
*		This functionality may be added later.
*/
namespace KE
{
	struct EventListener
	{
		s_ptr<Event> event;
		char priority;
	};

	struct EventListenerOperation
	{
		bool add;
		EventListener eventListener;
	};

	class EventHandler
	{
		private:
			bool skipEventType;
			u_ptr<std::unordered_map<ushort, u_ptr<std::vector<EventListener>>>> eventListeners;
			u_ptr<std::queue<EventListenerOperation>> listenerOperationQueue;

			virtual void ignoreSdlEvents() final;
			virtual void runPendingListenerOperations() final;
		public:
			EventHandler();
			~EventHandler();

			virtual void addEventListener(s_ptr<Event> event) final;
			virtual void addEventListener(s_ptr<Event> event, char priority) final;
			virtual void removeEventListener(s_ptr<Event> event) final;
			virtual void dispatchEvent(s_ptr<Event> event) final;
			virtual void skipCurrentEventType() final;
			virtual void update() final;
	};
}
