#pragma once

#include "ke/events/event.h"
#include "SDL_events.h"

namespace KE
{
	class MouseWheelEvent : public Event
	{
		protected:
			int x;
			int y;
			int verticalScrollAmount;
		public:
			MouseWheelEvent();
			~MouseWheelEvent();

			virtual bool isTriggered(const SDL_Event& sldEvent) final override;
	};
}