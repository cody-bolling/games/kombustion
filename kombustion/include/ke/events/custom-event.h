#pragma once

#include "ke/base/macros.h"
#include "ke/events/event.h"

namespace KE
{
	class CustomEvent : public Event
	{
		protected:
			int code;
			void* data1;
			void* data2;
		public:
			CustomEvent();
			CustomEvent(ushort type, int code, void* data1, void* data2);
			~CustomEvent();

			virtual operator SDL_Event() final override;
			virtual bool isTriggered(const SDL_Event& sdlEvent) final override;
	};
}