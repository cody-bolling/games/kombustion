#pragma once

#include "ke/base/macros.h"
#include "ke/events/event.h"

namespace KE
{
	class MouseButtonEvent : public Event
	{
		private:
			virtual void validateType() final;
		protected:
			uchar button;
			int x;
			int y;
		public:
			MouseButtonEvent();
			MouseButtonEvent(ushort type, uchar button);
			~MouseButtonEvent();

			virtual operator SDL_Event() final override;
			virtual bool isTriggered(const SDL_Event& sdlEvent) final override;
	};
}