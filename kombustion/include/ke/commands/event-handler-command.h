#pragma once

#include "ke/events/event-handler.h"
#include "ke/base/macros.h"

#include <cassert>

namespace KE
{
	class EventHandlerCommand
	{
		private:
			static u_ptr<EventHandler> instance;
		public:
			inline static void init(EventHandler* instance)
			{
				assert(!(EventHandlerCommand::instance));
				EventHandlerCommand::instance = u_ptr<EventHandler>(instance);
			}

			inline static void release(EventHandler* instance)
			{
				assert(EventHandlerCommand::instance.get() == instance);
				EventHandlerCommand::instance.release();
			}

			inline static void addEventListener(s_ptr<Event> event) { instance->addEventListener(event); }
			inline static void addEventListener(s_ptr<Event> event, char priority) { instance->addEventListener(event, priority); }
			inline static void removeEventListener(s_ptr<Event> event) { instance->removeEventListener(event); }
			inline static void dispatchEvent(s_ptr<Event> event) { instance->dispatchEvent(event); }
			inline static void skipCurrentEventType() { instance->skipCurrentEventType(); };
	};
}