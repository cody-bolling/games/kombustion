#pragma once

#include "ke/graphics/graphics-api.h"
#include "ke/base/macros.h"

#include <cassert>
#include <string>

namespace KE
{
	class GraphicsAPICommand
	{
		private:
			static u_ptr<GraphicsAPI> instance;
		public:
			inline static void init(GraphicsAPI* instance)
			{
				assert(!(GraphicsAPICommand::instance));
				GraphicsAPICommand::instance = u_ptr<GraphicsAPI>(instance);
			}

			inline static void release(GraphicsAPI* instance)
			{
				assert(GraphicsAPICommand::instance.get() == instance);
				GraphicsAPICommand::instance.release();
			}

			inline static void setClearColor(const glm::vec4& color) { instance->setClearColor(color); }
			inline static void clear() { instance->clear(); }
			inline static void setViewport(uint32_t x, uint32_t y, uint32_t width, uint32_t height) { instance->setViewport(x, y, width, height); }
			inline static u_ptr<VertexBuffer> createVertexBuffer(float* vertices, uint32_t size, std::initializer_list<BufferElement> elements) { return instance->createVertexBuffer(vertices, size, elements); }
			inline static u_ptr<IndexBuffer> createIndexBuffer(uint32_t* indices, uint32_t count) { return instance->createIndexBuffer(indices, count); }
			inline static u_ptr<VertexArray> createVertexArray() { return instance->createVertexArray(); }
			inline static void addShader(const std::string& filePath) { instance->addShader(filePath); }
			inline static void addShaderDirectory(const std::string& directoryPath) { instance->addShaderDirectory(directoryPath); }
	};
}
