#pragma once

#include "ke/graphics/user-interface/imgui/imgui-module.h"

namespace KE
{
	class ImGuiCommand
	{
		private:
			static u_ptr<ImGuiModule> instance;
		public:
			inline static void init(ImGuiModule* instance)
			{
				assert(!(ImGuiCommand::instance));
				ImGuiCommand::instance = u_ptr<ImGuiModule>(instance);
			}

			inline static void release(ImGuiModule* instance)
			{
				assert(ImGuiCommand::instance.get() == instance);
				ImGuiCommand::instance.release();
			}

			inline static bool processEvent(SDL_Event* sdlEvent) { return instance->processEvent(sdlEvent); }
			inline static void addImGuiWindow(s_ptr<ImGuiWindow> window) { instance->addImGuiWindow(window); }
			inline static void removeImGuiWindow(s_ptr<ImGuiWindow> window) { instance->removeImGuiWindow(window); }
	};
}