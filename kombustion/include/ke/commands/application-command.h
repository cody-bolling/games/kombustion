#pragma once

#include "ke/base/application.h"
#include "ke/base/macros.h"

#include <memory>

namespace KE
{
	class ApplicationCommand
	{
		private:
			static u_ptr<Application> instance;
		public:
			inline static void init(Application* instance)
			{
				assert(!(ApplicationCommand::instance));
				ApplicationCommand::instance = u_ptr<Application>(instance);
			}

			inline static void release(Application* instance)
			{
				assert(ApplicationCommand::instance.get() == instance);
				ApplicationCommand::instance.release();
			}

			inline static void stop() { instance->stop(); }
			inline static void log(std::string log) { if (instance != nullptr) instance->log(log); }
	};
}