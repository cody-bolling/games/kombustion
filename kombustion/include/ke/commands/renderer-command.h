#pragma once

#include "ke/graphics/renderer.h"
#include "ke/base/macros.h"

namespace KE
{
	class RendererCommand
	{
		private:
			static u_ptr<Renderer> instance;
		public:
			inline static void init(Renderer* instance)
			{
				assert(!(RendererCommand::instance));
				RendererCommand::instance = u_ptr<Renderer>(instance);
			}

			inline static void release(Renderer* instance)
			{
				assert(RendererCommand::instance.get() == instance);
				RendererCommand::instance.release();
			}

			inline static void clear() { instance->clear(); }
			inline static void tempDrawSquare(const VertexArray& vertexArray, const std::string& shaderName) { instance->tempDrawSquare(vertexArray, shaderName); }
	};
}