#include "ke/graphics/window.h"
#include "ke/graphics/opengl/opengl-window.h"
#include "ke/base/console-log.h"
#include "ke/commands/event-handler-command.h"
#include "ke/commands/graphics-api-command.h"

namespace KE
{
    class WindowResizeEvent : public WindowEvent
    {
        public:
            inline WindowResizeEvent() : WindowEvent((uchar)WindowEventType::RESIZED) {}
            inline ~WindowResizeEvent() {}

            virtual inline void function() final override { GraphicsAPICommand::setViewport(0, 0, data1, data2); }
    };

    bool Window::initialized = false;

    Window::Window()
        : window(nullptr),
          renderer(nullptr),
          imguiModule(nullptr),
          windowResizeEvent(std::make_shared<WindowResizeEvent>())
    {
        assert(!initialized);
        initialized = true;

        EventHandlerCommand::addEventListener(windowResizeEvent);
    }

    Window::~Window()
    {
        initialized = false;

        SDL_DestroyWindow(window);
        SDL_Quit();

        infoLog("Window", this, "deleted");
    }

    u_ptr<Window> Window::create(GraphicsPlatform graphicsPlatform)
    {
        u_ptr<Window> window;

        switch (graphicsPlatform)
        {
            case GraphicsPlatform::OPENGL:
                window = std::make_unique<OpenGLWindow>();
                break;
            default:
                errorLog("Must set the Graphics Platform to valid option.");
                return nullptr;
        }

        GraphicsAPICommand::setClearColor({ 0.07, 0.07, 0.07, 1.0 });

        return window;
    }

    void Window::preRender()
    {
        renderer->clear();
    }

    void Window::postRender()
    {
        imguiModule->render();
    }
}