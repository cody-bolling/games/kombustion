#include "ke/graphics/graphics-api.h"
#include "ke/commands/graphics-api-command.h"
#include "ke/base/console-log.h"

namespace KE
{
	u_ptr<GraphicsAPI> GraphicsAPICommand::instance = nullptr;

	GraphicsAPI::GraphicsAPI()
	{
		GraphicsAPICommand::init(this);
	}

	GraphicsAPI::~GraphicsAPI()
	{
		GraphicsAPICommand::release(this);
		infoLog("GraphicsAPI", this, "deleted");
	}
}
