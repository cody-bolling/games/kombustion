#include "ke/graphics/user-interface/imgui/imgui-module.h"
#include "ke/commands/imgui-command.h"
#include "ke/base/console-log.h"

#include <imgui_impl_sdl.h>

namespace KE
{
	u_ptr<ImGuiModule> ImGuiCommand::instance = nullptr;

	ImGuiModule::ImGuiModule()
		: io(init()),
		  pendingOperations(std::make_unique<std::queue<ImGuiWindowOperation>>()),
		  windows(std::make_unique<std::vector<s_ptr<ImGuiWindow>>>())
	{
		ImGuiCommand::init(this);

		ImGui::StyleColorsDark();
	}

	ImGuiModule::~ImGuiModule()
	{
		ImGui::DestroyContext();
		ImGuiCommand::release(this);
		infoLog("ImGuiModule deleted");
	}

	ImGuiIO& ImGuiModule::init()
	{
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGui::StyleColorsDark();
		ImGuiIO& io = ImGui::GetIO();
		io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

		// Until we add more windows, there's no need to enable docking for now
		// io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;

		return io;
	}

	void ImGuiModule::runPendingOperations()
	{
		while (pendingOperations->empty() == false)
		{
			bool eventFound = false;
			ImGuiWindowOperation operation = pendingOperations->front();
			pendingOperations->pop();

			if (operation.add)
			{
				auto end = windows->end();

				for (auto imguiWindow = windows->begin(); imguiWindow < end; imguiWindow++)
				{
					if (operation.window.get() == imguiWindow->get())
					{
						eventFound = true;
						break;
					}
				}

				if (eventFound)
				{
					warningLog("ImGuiWindow", operation.window.get(), "not added to ImGuiModule because it's already there.");
					continue;
				}

				windows->emplace_back(operation.window);
				continue;
			}

			auto end = windows->end();

			for (auto imguiWindow = windows->begin(); imguiWindow < end; imguiWindow++)
			{
				if (operation.window.get() == imguiWindow->get())
				{
					eventFound = true;
					windows->erase(imguiWindow);
					break;
				}
			}

			if (eventFound == false)
			{
				warningLog("ImGuiWindow", operation.window.get(), "not removed to ImGuiModule because it's not there.");
			}
		}
	}

	void ImGuiModule::render()
	{
		runPendingOperations();

		preRender();
		ImGui::NewFrame();

		auto end = windows->end();

		for (auto imguiWindow = windows->begin(); imguiWindow < end; imguiWindow++)
		{
			(*imguiWindow)->render();
		}

		ImGui::Render();
		postRender();
	}

	bool ImGuiModule::processEvent(SDL_Event* sdlEvent)
	{
		ImGui_ImplSDL2_ProcessEvent(sdlEvent);

		return (io.WantCaptureMouse || io.WantCaptureKeyboard);
	}

	void ImGuiModule::addImGuiWindow(s_ptr<ImGuiWindow> window)
	{
		if (window != nullptr)
		{
			pendingOperations->push({ true, window });
		}
	}

	void ImGuiModule::removeImGuiWindow(s_ptr<ImGuiWindow> window)
	{
		if (window != nullptr)
		{
			pendingOperations->push({ false, window });
		}
	}
}