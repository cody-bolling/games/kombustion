#include "ke/graphics/user-interface/imgui/imgui-opengl.h"

namespace KE
{
	OpenGLImGuiModule::OpenGLImGuiModule(OpenGLWindow* openglWindow)
		: openglWindow(openglWindow)
	{
		ImGui_ImplSDL2_InitForOpenGL(openglWindow->getSDLWindow(), openglWindow->getSDLOpenGLContext());
		ImGui_ImplOpenGL3_Init("#version 460");
	}

	OpenGLImGuiModule::~OpenGLImGuiModule()
	{
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplSDL2_Shutdown();
	}

	void OpenGLImGuiModule::preRender()
	{
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSDL2_NewFrame();
	}

	void OpenGLImGuiModule::postRender()
	{
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
		{
			SDL_GLContext backupContext = openglWindow->getSDLOpenGLContext();
			ImGui::UpdatePlatformWindows();
			ImGui::RenderPlatformWindowsDefault();
			SDL_GL_MakeCurrent(openglWindow->getSDLWindow(), backupContext);
		}
	}
}