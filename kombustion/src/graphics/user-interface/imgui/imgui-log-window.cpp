#pragma once

#include "ke/graphics/user-interface/imgui/imgui-log-window.h"

#include <sstream>

namespace KE
{
    LogWindow::LogWindow()
        : windowFlags(0)
    {
        windowFlags |= ImGuiWindowFlags_NoCollapse;
        clear();
    }

    LogWindow::~LogWindow() {}

    void LogWindow::clear()
    {
        Buf.clear();
        LineOffsets.clear();
        LineOffsets.push_back(0);
    }

    void LogWindow::log(std::string log)
    {
        int old_size = Buf.size();
        Buf.append(log.c_str(), log.c_str() + log.length());

        for (int new_size = Buf.size(); old_size < new_size; old_size++)
        {
            if (Buf[old_size] == '\n')
            {
                LineOffsets.push_back(old_size + 1);
            }
        }
    }

    void LogWindow::render()
    {
        ImGui::SetNextWindowPos(ImVec2(10, 10), ImGuiCond_FirstUseEver);
        ImGui::SetNextWindowSize(ImVec2(800, 400), ImGuiCond_FirstUseEver);

        ImGui::Begin("Log", NULL, windowFlags);

        ImGui::Text("%.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        ImGui::Separator();

        bool clear = ImGui::Button("Clear");
        ImGui::SameLine(ImGui::GetWindowWidth() - 260.0f);
        Filter.Draw("Filter", 200.0f);
        ImGui::Separator();

        if (ImGui::BeginChild("scrolling", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar))
        {
            if (clear)
            {
                LogWindow::clear();
            }

            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
            const char* buf = Buf.begin();
            const char* buf_end = Buf.end();

            if (Filter.IsActive())
            {
                for (int line_no = 0; line_no < LineOffsets.Size; line_no++)
                {
                    const char* line_start = buf + LineOffsets[line_no];
                    const char* line_end = (line_no + 1 < LineOffsets.Size) ? (buf + LineOffsets[line_no + 1] - 1) : buf_end;

                    if (Filter.PassFilter(line_start, line_end))
                    {
                        ImGui::TextUnformatted(line_start, line_end);
                    }
                }
            }
            else
            {
                ImGuiListClipper clipper;
                clipper.Begin(LineOffsets.Size);

                while (clipper.Step())
                {
                    for (int line_no = clipper.DisplayStart; line_no < clipper.DisplayEnd; line_no++)
                    {
                        const char* line_start = buf + LineOffsets[line_no];
                        const char* line_end = (line_no + 1 < LineOffsets.Size) ? (buf + LineOffsets[line_no + 1] - 1) : buf_end;
                        ImGui::TextUnformatted(line_start, line_end);
                    }
                }

                clipper.End();
            }

            ImGui::PopStyleVar();

            if (ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
            {
                ImGui::SetScrollHereY(1.0f);
            }
        }

        ImGui::EndChild();
        ImGui::End();
    }
}