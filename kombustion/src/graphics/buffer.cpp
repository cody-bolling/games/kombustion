#include "ke/graphics/buffer.h"
#include "ke/base/console-log.h"

namespace KE
{
	/*
	* Buffer Element
	*/

	BufferElement::BufferElement()
		: name(""),
		  type(ElementType::UNDEFINED),
		  datumSize(0),
		  datumCount(0),
		  normalized(false),
		  offset(0) {}

	BufferElement::BufferElement(const std::string& name, ElementType type, uint32_t datumCount, bool normalized)
		: name(name),
		  type(type),
		  datumSize(0),
		  datumCount(datumCount),
		  normalized(normalized),
		  offset(0)
	{
		switch (type)
		{
			case ElementType::BOOL:
				datumSize = 1;
				break;
			case ElementType::INT:
			case ElementType::FLOAT:
				datumSize = 4;
				break;
			default:
				errorLog("Must define a valid Element Type for the Buffer Element", this);
				break;
		}
	}

	/*
	* Vertex Buffer
	*/

	VertexBuffer::VertexBuffer(float* vertices, uint32_t size, std::initializer_list<BufferElement> elements)
		: bufferLayout(elements),
		  stride(0)
	{
		setBufferLayoutOffsets();
	}

	VertexBuffer::~VertexBuffer() {}

	void VertexBuffer::setBufferLayoutOffsets()
	{
		uint32_t offset = 0;

		for (BufferElement& element : bufferLayout)
		{
			element.offset = offset;
			offset += element.datumSize * element.datumCount;
			stride += element.datumSize * element.datumCount;
		}
	}

	/*
	* Index Buffer
	*/

	IndexBuffer::IndexBuffer(uint32_t* indices, uint32_t count)
		: count(count) {}

	IndexBuffer::~IndexBuffer() {}

	/*
	* Vertex Array
	*/

	VertexArray::VertexArray()
		: vertexBuffers({}),
		  indexBuffer(nullptr),
		  vertexBufferElementCount(0) {}

	VertexArray::~VertexArray() {}
}