#include "ke/graphics/renderer.h"
#include "ke/commands/renderer-command.h"
#include "ke/base/console-log.h"

namespace KE
{
	u_ptr<Renderer> RendererCommand::instance = nullptr;

	Renderer::Renderer()
	{
		RendererCommand::init(this);
	}

	Renderer::~Renderer()
	{
		RendererCommand::release(this);
		infoLog("Renderer", this, "deleted");
	}

	void Renderer::clear()
	{
		graphicsAPI->clear();
	}
}