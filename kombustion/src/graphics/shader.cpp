#include "ke/graphics/shader.h"
#include "ke/base/console-log.h"

#include <fstream>
#include <sstream>
#include <ranges>
#include <string_view>

namespace KE
{
	Shader::Shader(const std::string& filePath)
		: uniforms(std::make_unique<std::unordered_map<std::string, Uniform>>()) {}

	ShaderType Shader::stringToShaderType(const std::string& shaderType)
	{
		if (shaderType == "vertex")
		{
			return ShaderType::VERTEX;
		}

		if (shaderType == "fragment")
		{
			return ShaderType::FRAGMENT;
		}

		return ShaderType::UNDEFINED;
	}

	void Shader::parseUniform(const std::string& line)
	{
		auto splits = std::string_view{ line } | std::views::split(' ');
		UniformType uniformType = UniformType::UNDEFINED;
		uchar count = 0;

		for (const auto& split : splits)
		{
			auto string = std::string_view{ split.begin(), split.end() };
			count++;

			switch (count)
			{
				case 1:
					continue;
				case 2:
					if (string == "bool")
						uniformType = UniformType::BOOL;
					else if (string == "int")
						uniformType = UniformType::INT;
					else if (string == "ivec2")
						uniformType = UniformType::INT_VECTOR_2;
					else if (string == "ivec3")
						uniformType = UniformType::INT_VECTOR_3;
					else if (string == "ivec4")
						uniformType = UniformType::INT_VECTOR_4;
					else if (string == "float")
						uniformType = UniformType::FLOAT;
					else if (string == "vec2")
						uniformType = UniformType::FLOAT_VECTOR_2;
					else if (string == "vec3")
						uniformType = UniformType::FLOAT_VECTOR_3;
					else if (string == "vec4")
						uniformType = UniformType::FLOAT_VECTOR_4;
					else if (string == "mat2")
						uniformType = UniformType::MATRIX_2;
					else if (string == "mat3")
						uniformType = UniformType::MATRIX_3;
					else if (string == "mat4")
						uniformType = UniformType::MATRIX_4;
					else if (string == "sampler2D")
						uniformType = UniformType::SAMPLER_2D;

					continue;
				case 3:
				{
					Uniform u = { uniformType };
					std::string name = std::string(string);
					name.pop_back();
					uniforms->try_emplace(name, u);
					infoLog("Uniform", name, "added to shader", this);

					continue;
				}
				default:
					continue;
			}
		}
	}

	std::unordered_map<ShaderType, std::string> Shader::parseShaderSource(const std::string& filePath)
	{
		std::ifstream file(filePath);
		std::string line;
		std::stringstream stream;
		std::unordered_map<ShaderType, std::string> shaderSources;
		ShaderType shaderType = ShaderType::UNDEFINED;
		std::string typeToken = "#type";
		std::string uniformToken = "uniform";

		while (getline(file, line))
		{
			if (line.substr(0, typeToken.length()) == typeToken)
			{
				std::string streamString = stream.str();
				stream.str(std::string()); 
				stream.clear();

				if (streamString.empty() == false)
				{
					shaderSources.try_emplace(shaderType, streamString); 
				}

				shaderType = Shader::stringToShaderType(line.substr(typeToken.length() + 1, line.length()));

				if (shaderType == ShaderType::UNDEFINED)
				{
					errorLog("Unknown shader type in file", filePath);
				}

				continue;
			}

			if (line.substr(0, uniformToken.length()) == uniformToken)
			{
				parseUniform(line);
			}

			stream << line << "\n";
		}

		std::string streamString = stream.str();
		stream.str(std::string());
		stream.clear();
		file.close();

		if (streamString.empty() == false)
		{
			shaderSources.try_emplace(shaderType, streamString);
		}

		return shaderSources;
	}

	ShaderLibrary::ShaderLibrary()
		: shaders(std::make_unique<std::unordered_map<std::string, s_ptr<Shader>>>()) {}

	void ShaderLibrary::addShader(std::string filePath)
	{
		s_ptr<Shader> shader = createShader(filePath);
		std::filesystem::path path(filePath);
		std::string fileExtension = path.extension().string();
		
		if (path.extension().string() != ".glsl")
		{
			errorLog("The file", filePath, "is not a valid shader file. Please use a .glsl file.");
		}

		std::string fileName = path.filename().string();
		fileName = fileName.substr(0, fileName.length() - fileExtension.length());

		shaders->try_emplace(fileName, shader);

		infoLog("Shader", fileName, "(", shader.get(), ") added to shader library", this);
	}

	void ShaderLibrary::addShaderDirectory(std::string directoryPath)
	{
		for (const auto& file : std::filesystem::directory_iterator(directoryPath))
		{
			addShader(file.path().string());
		}
	}

	s_ptr<Shader> ShaderLibrary::getShader(std::string shaderName)
	{
		return shaders->find(shaderName)->second;
	}
}