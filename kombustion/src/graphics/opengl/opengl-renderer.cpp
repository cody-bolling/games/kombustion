#include "ke/graphics/opengl/opengl-renderer.h"
#include "ke/graphics/opengl/opengl.h"

#include <glad/glad.h>

namespace KE
{
	OpenGLRenderer::OpenGLRenderer()
	{
		graphicsAPI = std::make_unique<OpenGL>();
	}

	OpenGLRenderer::~OpenGLRenderer() {}

	void OpenGLRenderer::tempDrawSquare(const VertexArray& vertexArray, const std::string& shaderName)
	{
		graphicsAPI->getShader(shaderName)->bind();
		vertexArray.bind();
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	}
}