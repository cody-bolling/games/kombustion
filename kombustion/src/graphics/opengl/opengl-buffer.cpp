#include "ke/graphics/opengl/opengl-buffer.h"
#include "ke/base/console-log.h"

#include <glad/glad.h>

namespace KE
{
	/*
	* Vertex Buffer
	*/

	OpenGLVertexBuffer::OpenGLVertexBuffer(float* vertices, uint32_t size, std::initializer_list<BufferElement> elements)
		: VertexBuffer(vertices, size, elements)
	{
		glCreateBuffers(1, &rendererID);
		bind();
		glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
	}

	OpenGLVertexBuffer::~OpenGLVertexBuffer()
	{
		glDeleteBuffers(1, &rendererID);
	}

	void OpenGLVertexBuffer::bind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, rendererID);
	}

	void OpenGLVertexBuffer::unbind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	/*
	* Index Buffer
	*/

	OpenGLIndexBuffer::OpenGLIndexBuffer(uint32_t* indices, uint32_t count)
		: IndexBuffer(indices, count)
	{
		glCreateBuffers(1, &rendererID);
		bind();
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(uint32_t), indices, GL_STATIC_DRAW);
	}

	OpenGLIndexBuffer::~OpenGLIndexBuffer()
	{
		glDeleteBuffers(1, &rendererID);
	}

	void OpenGLIndexBuffer::bind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererID);
	}

	void OpenGLIndexBuffer::unbind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	/*
	* Vertex Array
	*/

	OpenGLVertexArray::OpenGLVertexArray()
	{
		glCreateVertexArrays(1, &rendererID);
	}

	OpenGLVertexArray::~OpenGLVertexArray()
	{
		glDeleteVertexArrays(1, &rendererID);
	}

	GLenum OpenGLVertexArray::getGLenumType(ElementType type)
	{
		switch (type)
		{
			case ElementType::BOOL:
				return GL_BOOL;
			case ElementType::INT:
				return GL_INT;
			case ElementType::FLOAT:
				return GL_FLOAT;
			default:
				return 0;
		}
	}

	void OpenGLVertexArray::bind() const
	{
		glBindVertexArray(rendererID);
	}

	void OpenGLVertexArray::unbind() const
	{
		glBindVertexArray(0);
	}

	void OpenGLVertexArray::addVertexBuffer(const s_ptr<VertexBuffer>& vertexBuffer)
	{
		bind();
		vertexBuffer->bind();

		auto iterator = vertexBuffer->getBufferLayoutIteratorBegin();
		auto end = vertexBuffer->getBufferLayoutIteratorEnd();

		for (; iterator != end; ++iterator)
		{
			if (iterator->type == ElementType::INT)
			{
				glEnableVertexAttribArray(vertexBufferElementCount);
				glVertexAttribIPointer(vertexBufferElementCount,
					iterator->datumCount,
					getGLenumType(iterator->type),
					vertexBuffer->getBufferLayoutStride(),
					(const void*)(size_t)iterator->offset);

				vertexBufferElementCount++;

				continue;
			}

			glEnableVertexAttribArray(vertexBufferElementCount);
			glVertexAttribPointer(vertexBufferElementCount,
				iterator->datumCount,
				getGLenumType(iterator->type),
				iterator->normalized ? GL_TRUE : GL_FALSE,
				vertexBuffer->getBufferLayoutStride(),
				(const void*)(size_t)iterator->offset);

			vertexBufferElementCount++;
		}

		vertexBuffers.emplace_back(vertexBuffer);
	}

	void OpenGLVertexArray::setIndexBuffer(const s_ptr<IndexBuffer>& indexBuffer)
	{
		bind();
		indexBuffer->bind();

		this->indexBuffer = indexBuffer;
	}
}
