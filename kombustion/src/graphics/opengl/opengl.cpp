#include "ke/graphics/opengl/opengl.h"
#include "ke/graphics/opengl/opengl-buffer.h"

#include <glad/glad.h>

namespace KE
{
	OpenGL::OpenGL()
	{
		shaderLibrary = std::make_unique<OpenGLShaderLibrary>();

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_LINE_SMOOTH);
	}

	void OpenGL::setClearColor(const glm::vec4& color)
	{
		glClearColor(color.r, color.g, color.b, color.a);
	}

	void OpenGL::clear()
	{
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	}

	void OpenGL::setViewport(uint32_t x, uint32_t y, uint32_t width, uint32_t height)
	{
		glViewport(x, y, width, height);
	}

	u_ptr<VertexBuffer> OpenGL::createVertexBuffer(float* vertices, uint32_t size, std::initializer_list<BufferElement> elements)
	{
		return std::make_unique<OpenGLVertexBuffer>(vertices, size, elements);
	}

	u_ptr<IndexBuffer> OpenGL::createIndexBuffer(uint32_t* indices, uint32_t count)
	{
		return std::make_unique<OpenGLIndexBuffer>(indices, count);
	}

	u_ptr<VertexArray> OpenGL::createVertexArray()
	{
		return std::make_unique<OpenGLVertexArray>();
	}

	void OpenGL::addShader(const std::string& filePath)
	{
		shaderLibrary->addShader(filePath);
	}

	void OpenGL::addShaderDirectory(const std::string& directoryPath)
	{
		shaderLibrary->addShaderDirectory(directoryPath);
	}

	s_ptr<Shader> OpenGL::getShader(const std::string& name)
	{
		return shaderLibrary->getShader(name);
	}
}