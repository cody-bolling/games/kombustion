#include "ke/graphics/opengl/opengl-window.h"
#include "ke/graphics/opengl/opengl-renderer.h"
#include "ke/graphics/user-interface/imgui/imgui-opengl.h"
#include "ke/base/console-log.h"

#include <glad/glad.h>

namespace KE
{
	OpenGLWindow::OpenGLWindow()
        : context(nullptr)
	{
        initSDL();
        initGlad();

        renderer = std::make_unique<OpenGLRenderer>();
        imguiModule = std::make_unique<OpenGLImGuiModule>(this);
	}

	OpenGLWindow::~OpenGLWindow()
	{
        SDL_GL_DeleteContext(context);
	}

    void OpenGLWindow::initSDL()
    {
        const int WIDTH = 1280;
        const int HEIGHT = 720;

        if (SDL_Init(SDL_INIT_VIDEO) != 0)
        {
            errorLog(SDL_GetError());
        }

        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

        SDL_WindowFlags windowFlags = (SDL_WindowFlags)(SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
        window = SDL_CreateWindow("game", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT, windowFlags);
        context = SDL_GL_CreateContext(window);
        SDL_GL_MakeCurrent(window, context);

        SDL_GL_SetSwapInterval(1);
    }

    static void GLAPIENTRY messageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
    {
        if (id == 131185)
        {
            return;
        }

        std::string typeString;
        std::string severityString;

        switch (type)
        {
            case GL_DEBUG_TYPE_ERROR:
                typeString = "ERROR";
                break;
            case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
                typeString = "DEPRECATED_BEHAVIOR";
                break;
            case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
                typeString = "UNDEFINED_BEHAVIOR";
                break;
            case GL_DEBUG_TYPE_PORTABILITY:
                typeString = "PORTABILITY";
                break;
            case GL_DEBUG_TYPE_PERFORMANCE:
                typeString = "PERFORMANCE";
                break;
            case GL_DEBUG_TYPE_OTHER:
                typeString = "OTHER";
                break;
        }

        switch (severity)
        {
            case GL_DEBUG_SEVERITY_LOW:
                severityString = "LOW";
                break;
            case GL_DEBUG_SEVERITY_MEDIUM:
                severityString = "MEDIUM";
                break;
            case GL_DEBUG_SEVERITY_HIGH:
                severityString = "HIGH";
                break;
        }

        infoLog("--- OPENGL MESSAGE ---", "\nType:", typeString, "\nSeverity:", severityString, "\nID:", id, "\nMessage:", message);

        if (typeString == "ERROR")
        {
            errorLog("OpenGL error, see above.");
        }
    }

    void OpenGLWindow::initGlad()
    {
        gladLoadGLLoader(SDL_GL_GetProcAddress);
        glDebugMessageCallback(messageCallback, 0);

        infoLog("OpenGL Version:", glGetString(GL_VERSION));
    }

    void OpenGLWindow::postRender()
    {
        Window::postRender();
        SDL_GL_SwapWindow(window);
    }
}