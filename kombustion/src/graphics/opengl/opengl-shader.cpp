#include "ke/graphics/opengl/opengl-shader.h"
#include "ke/base/console-log.h"

namespace KE
{
	OpenGLShader::OpenGLShader(const std::string& filePath)
		: Shader(filePath)
	{
		compile(parseShaderSource(filePath));
	}

	OpenGLShader::~OpenGLShader()
	{
		glDeleteProgram(rendererID);
	}

	void OpenGLShader::checkShaderCompileStatus(std::vector<GLuint> shaders)
	{
		GLint compiled = 0;

		GLuint shader = *(shaders.end() - 1);
		glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

		if (compiled == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
			std::vector<GLchar> infoLog(maxLength);
			glGetShaderInfoLog(shader, maxLength, &maxLength, &infoLog[0]);

			for (auto shader : shaders)
			{
				glDeleteShader(shader);
			}

			errorLog("Shader failed to compile.\n", infoLog.data());
		}
	}

	void OpenGLShader::checkShaderLinkStatus(GLuint program, std::vector<GLuint> shaders)
	{
		GLint linked = 0;
		glGetProgramiv(program, GL_LINK_STATUS, (int*)&linked);

		if (linked == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
			std::vector<GLchar> infoLog(maxLength);
			glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
			glDeleteProgram(program);

			for (auto shader : shaders)
			{
				glDeleteShader(shader);
			}

			errorLog("Shader failed to link.\n", infoLog.data());
		}
	}

	void OpenGLShader::compile(std::unordered_map<ShaderType, std::string> shaderSources)
	{
		rendererID = glCreateProgram();
		std::vector<GLuint> shaders;

		for (auto& keyValue : shaderSources)
		{
			GLuint shader;

			switch (keyValue.first)
			{
				case ShaderType::VERTEX:
					shader = glCreateShader(GL_VERTEX_SHADER);
					break;
				case ShaderType::FRAGMENT:
					shader = glCreateShader(GL_FRAGMENT_SHADER);
					break;
			}

			shaders.emplace_back(shader);
			const GLchar* source = keyValue.second.c_str();
			glShaderSource(shader, 1, &source, 0);
			glCompileShader(shader);
			checkShaderCompileStatus(shaders);
			glAttachShader((GLuint)rendererID, shader);
		}

		glLinkProgram((GLuint)rendererID);
		checkShaderLinkStatus((GLuint)rendererID, shaders);

		for (auto shader : shaders)
		{
			glDetachShader((GLuint)rendererID, shader);
		}
	}

	void OpenGLShader::bind() const
	{
		glUseProgram(rendererID);
	}

	void OpenGLShader::unbind() const
	{
		glUseProgram(0);
	}

	s_ptr<Shader> OpenGLShaderLibrary::createShader(std::string filePath)
	{
		return std::make_shared<OpenGLShader>(filePath);
	}
}