#include "ke/base/application.h"
#include "ke/commands/application-command.h"
#include "ke/commands/imgui-command.h"
#include "ke/base/console-log.h"

namespace KE
{
	class WindowCloseEvent : public WindowEvent
	{
		public:
			inline WindowCloseEvent()
				: WindowEvent((uchar)WindowEventType::CLOSE) {}

			virtual inline void function() final override
			{
				ApplicationCommand::stop();
			}
	};

	u_ptr<Application> ApplicationCommand::instance = nullptr;

	Application::Application()
	{
		ApplicationCommand::init(this);

		running = true;
		logWindow = std::make_shared<KE::LogWindow>();
		eventHandler = std::make_unique<EventHandler>();
		quitEvent = std::make_shared<QuitEvent>();
		windowCloseEvent = std::make_shared<WindowCloseEvent>();
		window = Window::create(GraphicsPlatform::OPENGL);
		updateTimer = std::make_unique<Timer>();
		maxUpdateDeltaTime = 20.0f;
		updateDeltaTime = 0.0f;

		eventHandler->addEventListener(quitEvent);
		eventHandler->addEventListener(windowCloseEvent);
		ImGuiCommand::addImGuiWindow(logWindow);
	}

	Application::~Application()
	{
		ApplicationCommand::release(this);
		infoLog("Application", this, "deleted");
	}

	void Application::run()
	{
		while (running)
		{
			updateTimer->reset();

			eventHandler->update();
			update();

			window->preRender();
			render();
			window->postRender();

			updateDeltaTime = updateTimer->elapsed();

			if (updateDeltaTime > maxUpdateDeltaTime)
			{
				updateDeltaTime = maxUpdateDeltaTime;
			}
		}
	}

	void Application::stop()
	{
		running = false;
	}

	void Application::log(std::string log)
	{
		logWindow->log(log);
	}
}
