#include "ke/base/game-object.h"
#include "ke/base/console-log.h"

namespace KE
{
	uint32_t GameObject::nextID = 0;

	GameObject::GameObject()
		: id(GameObject::generateID()) {}

	GameObject::~GameObject()
	{
		infoLog("Game object", this, "deleted");
	}

	uint32_t GameObject::getID()
	{
		return (uint32_t)id;
	}
}
