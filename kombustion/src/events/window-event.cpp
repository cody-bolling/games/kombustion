#include "ke/events/window-event.h"
#include "ke/events/event-types.h"

namespace KE
{
	WindowEvent::WindowEvent()
		: Event((ushort)EventType::WINDOW),
		  windowEventType((uchar)WindowEventType::UNDEFINED),
		  data1(0),
		  data2(0) {}

	WindowEvent::WindowEvent(uchar windowEventType)
		: Event((ushort)EventType::WINDOW),
		  windowEventType(windowEventType),
		  data1(0),
		  data2(0) {}

	WindowEvent::~WindowEvent() {}

	bool WindowEvent::isTriggered(const SDL_Event& sdlEvent)
	{
		if (Event::isTriggered(sdlEvent) && windowEventType == (uchar)sdlEvent.window.event)
		{
			data1 = sdlEvent.window.data1;
			data2 = sdlEvent.window.data2;

			return true;
		}

		return false;
	}
}
