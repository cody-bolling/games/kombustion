#include "ke/events/custom-event.h"
#include "ke/events/event-types.h"

namespace KE
{
	CustomEvent::CustomEvent()
		: code(0),
		  data1(nullptr),
		  data2(nullptr) {}

	CustomEvent::CustomEvent(ushort type, int code, void* data1, void* data2)
		: Event(type),
		  code(code),
		  data1(data1),
		  data2(data2) {}

	CustomEvent::~CustomEvent() {}

	CustomEvent::operator SDL_Event()
	{
		SDL_Event sdlEvent;
		sdlEvent.type = type;
		sdlEvent.user.code = code;
		sdlEvent.user.data1 = data1;
		sdlEvent.user.data2 = data2;

		return sdlEvent;
	}

	bool CustomEvent::isTriggered(const SDL_Event& sdlEvent)
	{
		if (Event::isTriggered(sdlEvent))
		{
			return ((code == sdlEvent.user.code) && (data1 == sdlEvent.user.data1) && (data2 == sdlEvent.user.data2));
		}

		return false;
	}
}
