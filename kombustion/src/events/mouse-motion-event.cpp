#include "ke/events/mouse-motion-event.h"
#include "ke/events/event-types.h"
#include "ke/base/macros.h"

namespace KE
{
	MouseMotionEvent::MouseMotionEvent()
		: Event((ushort)EventType::MOUSE_MOTION),
		  x(0),
		  y(0) {}

	MouseMotionEvent::~MouseMotionEvent() {}

	bool MouseMotionEvent::isTriggered(const SDL_Event& sdlEvent)
	{
		if (Event::isTriggered(sdlEvent))
		{
			x = sdlEvent.button.x;
			y = sdlEvent.button.y;

			return true;
		}

		return false;
	}
}
