#include "ke/events/mouse-button-event.h"
#include "ke/events/event-types.h"
#include "ke/base/console-log.h"

namespace KE
{
	MouseButtonEvent::MouseButtonEvent()
		: button((uchar)MouseButton::UNDEFINED),
		  x(0),
		  y(0) {}

	MouseButtonEvent::MouseButtonEvent(ushort type, uchar button)
		: Event(type),
		  button(button),
		  x(0),
		  y(0)
	{
		validateType();
	}

	MouseButtonEvent::~MouseButtonEvent() {}

	void MouseButtonEvent::validateType()
	{
		bool valid = (type == (ushort)EventType::MOUSE_BUTTON_PRESSED || type == (ushort)EventType::MOUSE_BUTTON_RELEASED);

		if (valid == false)
		{
			errorLog("Invalid MouseButtonEvent type. Please use EventType::MOUSE_BUTTON_PRESSED or EventType::MOUSE_BUTTON_RELEASED.");
		}
	}

	MouseButtonEvent::operator SDL_Event()
	{
		SDL_Event sdlEvent;
		sdlEvent.type = type;
		sdlEvent.button.button = button;

		return sdlEvent;
	}

	bool MouseButtonEvent::isTriggered(const SDL_Event& sdlEvent)
	{
		if (Event::isTriggered(sdlEvent) && (button == sdlEvent.button.button))
		{
			x = sdlEvent.button.x;
			y = sdlEvent.button.y;

			return true;
		}

		return false;
	}
}
