#include "ke/events/mouse-wheel-event.h"
#include "ke/events/event-types.h"
#include "ke/base/macros.h"

namespace KE
{
	MouseWheelEvent::MouseWheelEvent()
		: Event((ushort)EventType::MOUSE_WHEEL),
		  x(0),
		  y(0),
		  verticalScrollAmount(0) {}

	MouseWheelEvent::~MouseWheelEvent() {}

	bool MouseWheelEvent::isTriggered(const SDL_Event& sdlEvent)
	{
		if (Event::isTriggered(sdlEvent))
		{
			x = sdlEvent.wheel.mouseX;
			y = sdlEvent.wheel.mouseY;
			verticalScrollAmount = sdlEvent.wheel.y;

			return true;
		}

		return false;
	}
}
