#include "ke/events/event.h"
#include "ke/events/event-types.h"
#include "ke/base/console-log.h"

namespace KE
{
	Event::Event()
		: type((ushort)EventType::UNDEFINED),
		  disabled(false) {}

	Event::Event(ushort type)
		: type(type),
		  disabled(false) {}

	Event::~Event()
	{
		infoLog("Event", this, "deleted");
	}

	void Event::enable()
	{
		disabled = false;
	}

	void Event::disable()
	{
		disabled = true;
	}

	bool Event::isEnabled()
	{
		return !disabled;
	}

	ushort Event::getType()
	{
		return type;
	}

	Event::operator SDL_Event()
	{
		SDL_Event sdlEvent;
		sdlEvent.type = type;

		return sdlEvent;
	}

	bool Event::isTriggered(const SDL_Event& sdlEvent)
	{
		return (type == sdlEvent.type && !disabled);
	}
}
