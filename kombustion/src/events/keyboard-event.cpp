#include "ke/events/keyboard-event.h"
#include "ke/events/event-types.h"
#include "ke/base/console-log.h"

namespace KE
{
	Uint16 KeyboardEvent::modsToIgnore[6] = { KMOD_SCROLL, KMOD_MODE, KMOD_CAPS, KMOD_NUM, KMOD_RGUI, KMOD_LGUI };

	KeyboardEvent::KeyboardEvent()
		: keycode((uint)KeyCode::UNDEFINED),
		  mod((ushort)KeyMod::UNDEFINED) {}

	KeyboardEvent::KeyboardEvent(ushort type, uint keycode)
		: Event(type),
		  keycode(keycode),
		  mod((ushort)KeyMod::UNDEFINED)
	{
		validateType();
	}

	KeyboardEvent::KeyboardEvent(ushort type, uint keycode, ushort mod)
		: Event(type),
		  keycode(keycode),
		  mod(mod)
	{
		validateType();
	}

	KeyboardEvent::~KeyboardEvent() {}

	void KeyboardEvent::validateType()
	{
		bool valid = (type == (ushort)EventType::KEY_PRESSED || type == (ushort)EventType::KEY_RELEASED);

		if (valid == false)
		{
			errorLog("Invalid KeyboardEvent type. Please use EventType::KEY_DOWN or EventType::KEY_UP");
		}
	}

	ushort KeyboardEvent::ignoreModKeys(Uint16 mod)
	{
		for (uchar i = 0; i < 6; ++i)
		{
			if (mod >= modsToIgnore[i])
			{
				mod -= modsToIgnore[i];
			}
		}

		return (ushort)mod;
	}

	KeyboardEvent::operator SDL_Event()
	{
		SDL_Event sdlEvent;
		sdlEvent.type = type;
		sdlEvent.key.keysym.sym = keycode;
		sdlEvent.key.keysym.mod = mod;

		return sdlEvent;
	}

	bool KeyboardEvent::isTriggered(const SDL_Event& sdlEvent)
	{
		if (Event::isTriggered(sdlEvent) && sdlEvent.key.repeat == 0)
		{
			return ((keycode == sdlEvent.key.keysym.sym) && (mod == sdlEvent.key.keysym.mod));
		}

		return false;
	}
}
