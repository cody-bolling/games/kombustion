#include "ke/events/event-handler.h"
#include "ke/commands/event-handler-command.h"
#include "ke/commands/imgui-command.h"
#include "ke/base/console-log.h"

#include <SDL_events.h>

namespace KE
{
    u_ptr<EventHandler> EventHandlerCommand::instance = nullptr;

    EventHandler::EventHandler()
        : skipEventType(false),
          eventListeners(std::make_unique<std::unordered_map<ushort, u_ptr<std::vector<EventListener>>>>()),
          listenerOperationQueue(std::make_unique<std::queue<EventListenerOperation>>())
    {
        EventHandlerCommand::init(this);

        ignoreSdlEvents();
    }

    EventHandler::~EventHandler()
    {
        EventHandlerCommand::release(this);
        infoLog("Event handler", this, "deleted");
    }

    void EventHandler::ignoreSdlEvents()
    {
        /*
        * Enabled events
        */
        SDL_EventState(SDL_QUIT, SDL_ENABLE);
        SDL_EventState(SDL_WINDOWEVENT, SDL_ENABLE);
        SDL_EventState(SDL_KEYDOWN, SDL_ENABLE);
        SDL_EventState(SDL_KEYUP, SDL_ENABLE);
        SDL_EventState(SDL_MOUSEMOTION, SDL_ENABLE);
        SDL_EventState(SDL_MOUSEBUTTONDOWN, SDL_ENABLE);
        SDL_EventState(SDL_MOUSEBUTTONUP, SDL_ENABLE);
        SDL_EventState(SDL_MOUSEWHEEL, SDL_ENABLE);
        SDL_EventState(SDL_USEREVENT, SDL_ENABLE);

        /*
        * Potentailly add support later
        */
        SDL_EventState(SDL_DISPLAYEVENT, SDL_IGNORE);
        SDL_EventState(SDL_TEXTEDITING, SDL_IGNORE);
        SDL_EventState(SDL_TEXTINPUT, SDL_IGNORE);
        SDL_EventState(SDL_TEXTEDITING_EXT, SDL_IGNORE);
        SDL_EventState(SDL_CLIPBOARDUPDATE, SDL_IGNORE);
        SDL_EventState(SDL_AUDIODEVICEADDED, SDL_IGNORE);
        SDL_EventState(SDL_AUDIODEVICEREMOVED, SDL_IGNORE);

        /*
        * Mobile platform events
        */
        SDL_EventState(SDL_APP_TERMINATING, SDL_IGNORE);
        SDL_EventState(SDL_APP_LOWMEMORY, SDL_IGNORE);
        SDL_EventState(SDL_APP_WILLENTERBACKGROUND, SDL_IGNORE);
        SDL_EventState(SDL_APP_DIDENTERBACKGROUND, SDL_IGNORE);
        SDL_EventState(SDL_APP_WILLENTERFOREGROUND, SDL_IGNORE);
        SDL_EventState(SDL_APP_DIDENTERFOREGROUND, SDL_IGNORE);
        SDL_EventState(SDL_FINGERDOWN, SDL_IGNORE);
        SDL_EventState(SDL_FINGERUP, SDL_IGNORE);
        SDL_EventState(SDL_FINGERMOTION, SDL_IGNORE);
        SDL_EventState(SDL_DOLLARGESTURE, SDL_IGNORE);
        SDL_EventState(SDL_DOLLARRECORD, SDL_IGNORE);
        SDL_EventState(SDL_MULTIGESTURE, SDL_IGNORE);

        /*
        * Controller/joystick events
        */
        SDL_EventState(SDL_JOYAXISMOTION, SDL_IGNORE);
        SDL_EventState(SDL_JOYBALLMOTION, SDL_IGNORE);
        SDL_EventState(SDL_JOYHATMOTION, SDL_IGNORE);
        SDL_EventState(SDL_JOYBUTTONDOWN, SDL_IGNORE);
        SDL_EventState(SDL_JOYBUTTONUP, SDL_IGNORE);
        SDL_EventState(SDL_JOYDEVICEADDED, SDL_IGNORE);
        SDL_EventState(SDL_JOYDEVICEREMOVED, SDL_IGNORE);
        SDL_EventState(SDL_JOYBATTERYUPDATED, SDL_IGNORE);
        SDL_EventState(SDL_CONTROLLERAXISMOTION, SDL_IGNORE);
        SDL_EventState(SDL_CONTROLLERBUTTONDOWN, SDL_IGNORE);
        SDL_EventState(SDL_CONTROLLERBUTTONUP, SDL_IGNORE);
        SDL_EventState(SDL_CONTROLLERDEVICEADDED, SDL_IGNORE);
        SDL_EventState(SDL_CONTROLLERDEVICEREMOVED, SDL_IGNORE);
        SDL_EventState(SDL_CONTROLLERDEVICEREMAPPED, SDL_IGNORE);
        SDL_EventState(SDL_CONTROLLERTOUCHPADDOWN, SDL_IGNORE);
        SDL_EventState(SDL_CONTROLLERTOUCHPADMOTION, SDL_IGNORE);
        SDL_EventState(SDL_CONTROLLERTOUCHPADUP, SDL_IGNORE);
        SDL_EventState(SDL_CONTROLLERSENSORUPDATE, SDL_IGNORE);

        /*
        * File events
        */
        SDL_EventState(SDL_DROPFILE, SDL_IGNORE);
        SDL_EventState(SDL_DROPTEXT, SDL_IGNORE);
        SDL_EventState(SDL_DROPBEGIN, SDL_IGNORE);
        SDL_EventState(SDL_DROPCOMPLETE, SDL_IGNORE);

        /*
        * Misc events
        */
        SDL_EventState(SDL_LOCALECHANGED, SDL_IGNORE);
        SDL_EventState(SDL_SYSWMEVENT, SDL_IGNORE);
        SDL_EventState(SDL_KEYMAPCHANGED, SDL_IGNORE);
        SDL_EventState(SDL_SENSORUPDATE, SDL_IGNORE);
        SDL_EventState(SDL_RENDER_TARGETS_RESET, SDL_IGNORE);
        SDL_EventState(SDL_RENDER_DEVICE_RESET, SDL_IGNORE);
    }

    void EventHandler::runPendingListenerOperations()
    {
        while (listenerOperationQueue->empty() == false)
        {
            EventListenerOperation operation = listenerOperationQueue->front();
            ushort type = operation.eventListener.event->getType();

            if (operation.add)
            {
                auto listeners = eventListeners->try_emplace(type, std::make_unique<std::vector<EventListener>>()).first->second.get();
                auto end = listeners->end();
                std::vector<EventListener>::iterator insertLocation = listeners->begin();
                bool eventFound = false;

                for (auto listener = listeners->begin(); listener < end; ++listener)
                {
                    if (operation.eventListener.priority <= listener->priority)
                    {
                        insertLocation++;
                    }

                    if (operation.eventListener.event == listener->event)
                    {
                        eventFound = true;
                        break;
                    }
                }

                if (eventFound)
                {
                    warningLog("Not adding event", operation.eventListener.event.get(), "as it already exists in event handler");
                    listenerOperationQueue->pop();
                    continue;
                }

                infoLog("Event", operation.eventListener.event, "of type", type, "added to event handler");
                listeners->insert(insertLocation, operation.eventListener);
                listenerOperationQueue->pop();
                continue;
            }

            auto search = eventListeners->find(type);
            if (search == eventListeners->end())
            {
                warningLog("Cannot remove event", operation.eventListener.event.get(), "from event handler as it is not in the event handler");
                listenerOperationQueue->pop();
                continue;
            }

            auto listeners = search->second.get();
            auto end = listeners->end();
            std::vector<EventListener>::iterator listener;

            for (listener = listeners->begin(); listener < end; ++listener)
            {
                if (operation.eventListener.event == listener->event)
                {
                    listeners->erase(listener);
                    infoLog("Event", operation.eventListener.event, "of type", type, "removed from event handler");
                    break;
                }
            }

            if (listener == end)
            {
                warningLog("Cannot remove event", operation.eventListener.event.get(), "from event handler as it is not in the event handler");
            }

            listenerOperationQueue->pop();
        }
    }

    void EventHandler::addEventListener(s_ptr<Event> event)
    {
        if (event != nullptr)
        {
            listenerOperationQueue->push({ true, event, 0 });
        }
    }

    void EventHandler::addEventListener(s_ptr<Event> event, char priority)
    {
        if (event != nullptr)
        {
            listenerOperationQueue->push({ true, event, priority});
        }
    }

    void EventHandler::removeEventListener(s_ptr<Event> event)
    {
        if (event != nullptr)
        {
            listenerOperationQueue->push({ false, event });
        }
    }

    void EventHandler::dispatchEvent(s_ptr<Event> event)
    {
        if (event != nullptr)
        {
            SDL_Event sdlEvent = *event;

            infoLog("Event", event.get(), "dispatched");

            SDL_PushEvent(&sdlEvent);
        }
    }

    void EventHandler::skipCurrentEventType()
    {
        skipEventType = true;
    }

    void EventHandler::update()
    {
        runPendingListenerOperations();

        SDL_Event sdlEvent;

        while (SDL_PollEvent(&sdlEvent))
        {
            if (ImGuiCommand::processEvent(&sdlEvent))
            {
                continue;
            }

            auto search = eventListeners->find((ushort)sdlEvent.type);

            if (search == eventListeners->end())
            {
                continue;
            }

            std::vector<EventListener>::iterator listener;
            auto listeners = search->second.get();
            auto end = listeners->end();

            for (listener = listeners->begin(); listener < end; ++listener)
            {
                if (listener->event->isTriggered(sdlEvent))
                {
                    infoLog("Event", listener->event.get(), "of type", listener->event->getType(), "triggered");
                    listener->event->function();

                    if (skipEventType == true)
                    {
                        skipEventType = false;
                        break;
                    }
                }
            }
        }
    }
}
