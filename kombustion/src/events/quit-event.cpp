#include "ke/events/quit-event.h"
#include "ke/events/event-types.h"
#include "ke/commands/application-command.h"
#include "ke/base/macros.h"

namespace KE
{
	QuitEvent::QuitEvent()
		: Event((ushort)EventType::QUIT) {}

	QuitEvent::~QuitEvent() {}

	void QuitEvent::function()
	{
		ApplicationCommand::stop();
	}
}
